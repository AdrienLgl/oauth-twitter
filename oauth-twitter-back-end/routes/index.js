var express = require('express');
var router = express.Router();
const OAuthController = require('../controllers/oauthcontroller');
const OAuthServer = require('express-oauth-server');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Oauth-Twitter' });
});
router.oauth = new OAuthServer({
  model: OAuthController
});
router.post('/oauth/token', router.oauth.token());
router.post('/oauth/set_client', function (req, res, next) {
  OAuthController.setClient(req.body).then((client) => res.json(client)).catch((err) => {
    return next(err);
  });
});
router.post('/oauth/signup', function (req, res, next) {
  OAuthController.setUser(req.body).then((user) => res.json(user)).catch((err) => {
    return next(err);
  });
});
router.get('/ressources', router.oauth.authenticate(), function (req, res) {
  res.json('Ce message est généré par le back-end et indique que votre token est valide et que vous avez accès aux ressources disponible sur le serveur');
});
router.post('/oauth/login', function (req, res, next) {
  OAuthController.getUser(req.body.username, req.body.password).then((user) => {
    if (user.status && user.user !== null) {
      res.json(user);
    } else {
      res.status(user.error.code).send(user);
    }
  }).catch((error) => {
    return next(error);
  });
});
module.exports = router;

# OAuth Twitter V1

### Introduction

Visually reproduce as best as possible a connection interface of an existing application. Authentication must be based on the OAuth2 process.

### Get Started

Requirements :


- Angular (v12.0 +)
- Node JS (v16.15.1)
- PostgreSQL
- Sequelize (v6.20.1) *optional*

You can find two projects in this repository :
- Front-End (Angular)
- Back-End (NodeJs / Express)

## Back-End

1) Open project, launch ```npm i``` command to install dependencies.
2) Install PostgreSQL if necessary and create a new database : ```oauth_twitter```.
3) If you want, you can create a ```config.json``` file in a ```config``` directory at the project root and execute the command ```sequelize db:migrate``` (always at the project root).

config.json :
```json
{
  "development": {
    "username": "your postgres user",
    "password": "your password",
    "database": "oauth_twitter",
    "host": "127.0.0.1",
    "dialect": "postgres"
  },
  "test": {
    "username": "your postgres user",
    "password": "your password",
    "database": "oauth_twitter",
    "host": "127.0.0.1",
    "dialect": "postgres"
  },
  "production": {
    "username": "your postgres user",
    "password": "your password",
    "database": "oauth_twitter",
    "host": "127.0.0.1",
    "dialect": "postgres"
  }
}
```

This command will create all tables in database.

Otherwise, execute installation [script](./oauth-twitter-database/) in PostgreSQL to deploy database.

4) Launch ```nodemon``` to run server.
5) If project initialization is correct, script will display :

```sh
[nodemon] starting `node ./bin/www`
Checking database connection...
Server is starting... Please wait
Server listening on http://localhost:3000
Executing (default): SELECT 1+1 AS result
Connected to database successfully !
```


## Front-End

1) Open project, launch ```npm i``` command to install dependencies.
2) Check back-end address *(default: localhost:3000)* and change if necessary in the ```proxy.conf.json``` file.
3) Client codes are already generated in database but if you doesn't use the installation script provided with this repository, don't forget to create a new [client](#specs) and update those values in ```environnement.ts```.
4) ```npm start``` to launch application.


## Credentials

You can create new account or use an account already created :

```
Username: mehidine.chupeau
Password: mehidine.chupeau
```

## Specs

```{{your domain}}/oauth/set_client```

Allows you to add a new client 

```curl
curl --location --request POST 'http://localhost:3000/oauth/set_client' \
--header 'Content-Type: application/json' \
--data-raw '{
    "clientId": "oauth_twitter",
    "clientSecret": "",
    "redirectUris": "http://localhost:3000/oauth/callback",
    "grants": ["password", "refresh_token"]
}'
```

```{{your domain}}/oauth/signup```

Allows you to create a new user

- Authentication : client_Id & client_Secret

curl --location --request POST 'http://localhost:3000/oauth/signup' \
--header 'Content-Type: application/json' \
--header 'Authorization: Basic TVFEdTFBL0FEUld5eGNOSE1KdEFTWFdPV3krdS9ORmorS0FFU09zQkxGZz06NWY2ODFlZmU1ZTc2ODE1YWNjMmM4NTE0OGE2ODdlYTcwMTY0ODA5YTZhNmE3NTJhMzY3ZjMxODgzNmU1OGU4ZGEwMmQ5YzI4ODk2MGQzOTU5NjYyM2Y1M2M1ZjI1YmNlZmIzOWQyNmU3OWFiNGRhMGFlYTk5NGNjMzhhNTY2M2M=' \
--data-raw '{
    "username": "root",
    "password": "root",
    "name": "root"
}'

```{{your domain}}/oauth/token```

Allows you to get user token

- Authentication : client_Id & client_Secret

curl --location --request POST 'http://localhost:3000/oauth/token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--header 'Authorization: Basic TVFEdTFBL0FEUld5eGNOSE1KdEFTWFdPV3krdS9ORmorS0FFU09zQkxGZz06NWY2ODFlZmU1ZTc2ODE1YWNjMmM4NTE0OGE2ODdlYTcwMTY0ODA5YTZhNmE3NTJhMzY3ZjMxODgzNmU1OGU4ZGEwMmQ5YzI4ODk2MGQzOTU5NjYyM2Y1M2M1ZjI1YmNlZmIzOWQyNmU3OWFiNGRhMGFlYTk5NGNjMzhhNTY2M2M=' \
--data-urlencode 'username=root' \
--data-urlencode 'grant_type=password' \
--data-urlencode 'password=root'

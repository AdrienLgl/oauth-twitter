// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  CLIENT_ID: 'MQDu1A/ADRWyxcNHMJtASXWOWy+u/NFj+KAESOsBLFg=',
  CLIENT_SECRET: '5f681efe5e76815acc2c85148a687ea70164809a6a6a752a367f318836e58e8da02d9c288960d39596623f53c5f25bcefb39d26e79ab4da0aea994cc38a5663c'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.

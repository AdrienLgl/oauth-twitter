export interface LoginResponse {
    access_token: string;
    token_type: string;
    expires_in: number;
    refresh_token: string;
}

export interface Login {
    username: string;
    grant_type: string;
    password: string;
}

export interface SignupResponse {
    id: number;
    username: string;
    password: string;
    name: string;
    updatedAt: string;
    createdAt: string;
}

export interface Signup {
    username: string;
    password: string;
    name: string;
}
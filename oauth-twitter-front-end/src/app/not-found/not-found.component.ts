import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['../login/login.component.scss', './not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer, private authService: AuthService, private fb: FormBuilder) {
    this.matIconRegistry.addSvgIcon(
      "notFound",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../assets/not_found.svg")
    );
  }

  ngOnInit(): void {
  }

}

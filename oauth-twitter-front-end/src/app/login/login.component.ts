import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthService } from '../_services/auth.service';

export interface LoginFormGroup {
  username: FormControl<string>;
  password: FormControl<string>;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errorState = false;
  errorMessage?: string;
  loading: boolean = false;
  loginForm = new FormGroup<LoginFormGroup>({
    username: new FormControl<string>('', { nonNullable: true, validators: [Validators.required] }),
    password: new FormControl<string>('', { nonNullable: true, validators: [Validators.required] }),
  });

  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer, private authService: AuthService, private fb: FormBuilder) {
    this.matIconRegistry.addSvgIcon(
      "logo",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../assets/twitter.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "google",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../assets/google.svg")
    );
  }

  ngOnInit(): void {
  }

  login(): void {
    const username = this.loginForm.controls.username.value;
    const password = this.loginForm.controls.password.value;
    if (this.checkFormValidity()) {
      this.loading = true;
      this.authService.login(username, password).subscribe((response: any) => {
        if (response) {
          this.errorState = false;
          this.authService.setSession(username, response);
        }
        setTimeout(() => {
          this.loading = false;
        }, 1000);
      }, (error) => {
        this.errorState = true;
        this.errorMessage = 'Identifiants invalides, veuillez réessayer';
        this.loading = false;
      });
    }
  }

  checkFormValidity(): boolean {
    this.loginForm.updateValueAndValidity();
    if (this.loginForm.valid) {
      this.errorState = false;
      return true;
    } else {
      this.errorState = true;
      this.errorMessage = 'Veuillez compléter le formulaire';
      return false;
    }
  }

}

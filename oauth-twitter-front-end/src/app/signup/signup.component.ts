import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Signup } from '../_models/auth';
import { AuthService } from '../_services/auth.service';

export interface SignupFormGroup {
  username: FormControl<string>;
  name: FormControl<string>;
  password: FormControl<string>;
  confirmPassword: FormControl<string>;
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss', '../login/login.component.scss']
})
export class SignupComponent implements OnInit {

  errorState = false;
  validState = false;
  validMessage?: string;
  errorMessage?: string;
  loading: boolean = false;

  signupForm = new FormGroup<SignupFormGroup>({
    username: new FormControl<string>('', { nonNullable: true, validators: [Validators.required] }),
    password: new FormControl<string>('', { nonNullable: true, validators: [Validators.required] }),
    name: new FormControl<string>('', { nonNullable: true, validators: [Validators.required] }),
    confirmPassword: new FormControl<string>('', { nonNullable: true, validators: [Validators.required] })
  });

  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer, private authService: AuthService, private router: Router) {
    this.matIconRegistry.addSvgIcon(
      "logo",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../assets/twitter.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "google",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../assets/google.svg")
    );
  }

  ngOnInit(): void {
    this.signupForm.controls.confirmPassword.addValidators(this.checkPasswords);
  }

  checkPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    let pass = this.signupForm.controls.password.value;
    let confirmPass = this.signupForm.controls.confirmPassword.value;
    return pass === confirmPass ? null : { notSame: true }
  }

  signup(): void {
    this.signupForm.updateValueAndValidity();
    if (this.signupForm.valid) {
      const user: Signup = {
        username: this.signupForm.controls.username.value,
        password: this.signupForm.controls.password.value,
        name: this.signupForm.controls.name.value
      };
      this.loading = true;
      this.authService.signup(user).subscribe((response: any) => {
        if (response) {
          this.errorState = false;
          this.validState = true;
          this.validMessage = 'Votre compte a été crée avec succès !';
          this.loading = false;
          setTimeout(() => {
            this.router.navigate(['/login']);
          }, 2000);
        }

        setTimeout(() => {
          this.loading = false;
        }, 3000);
      }, (error) => {
        this.errorState = true;
        this.loading = false;
        this.errorMessage = 'Erreur lors de la création du compte, veuillez réessayer';
      });
    }

  }

}

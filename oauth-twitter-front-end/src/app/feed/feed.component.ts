import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss', '../login/login.component.scss']
})
export class FeedComponent implements OnInit {

  message?: string;

  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer, private authService: AuthService, private router: Router) {
    this.matIconRegistry.addSvgIcon(
      "logo",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../assets/twitter.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "google",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../assets/google.svg")
    );
  }

  ngOnInit(): void {
    this.getMessage();
  }

  logout(): void {
    this.authService.logout();
  }

  getMessage(): void {
    this.authService.getInformations().subscribe((response: any) => {
      this.message = response;
    });
  }

}

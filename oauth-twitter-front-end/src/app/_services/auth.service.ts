import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginResponse, Signup, SignupResponse } from '../_models/auth';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

const OAUTH_CLIENT = environment.CLIENT_ID;
const OAUTH_SECRET = environment.CLIENT_SECRET;
const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
    Authorization: 'Basic ' + btoa(OAUTH_CLIENT + ':' + OAUTH_SECRET)
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }

  login(username: string, password: string): Observable<LoginResponse> {
    const body = new HttpParams()
      .set('username', username)
      .set('password', password)
      .set('grant_type', 'password');
    return this.http.post<LoginResponse>('/api/oauth/token', body, HTTP_OPTIONS);
  }

  signup(user: Signup): Observable<SignupResponse> {
    return this.http.post<SignupResponse>('/api/oauth/signup', user);
  }

  getInformations(): Observable<string> {
    return this.http.get<string>('/api/ressources');
  }

  setSession(username: string, login: LoginResponse): void {
    localStorage.setItem('username', username);
    localStorage.setItem('id_token', login.access_token);
    localStorage.setItem('refresh_token', login.refresh_token);
    const date = new Date();
    date.setSeconds(date.getSeconds() + login.expires_in);
    localStorage.setItem('expiration', date.toString());
    this.router.navigate(['/feed']);
  }

  isLoggedIn(): boolean {
    const token = localStorage.getItem('id_token');
    if (token === null) {
      return false;
    }
    const expiration = localStorage.getItem('expiration');
    const time: Date = expiration !== null ? new Date(expiration) : new Date();
    const date: Date = new Date();
    if (time < date) {
      return false;
    }
    return true;
  }

  logout(): void {
    localStorage.removeItem('username');
    localStorage.removeItem('id_token');
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('expiration');
    this.router.navigate(['/login']);
  }
}
